package ru.tsc.marfin.tm.api.service;

import ru.tsc.marfin.tm.model.Command;

public interface ICommandService {
    Command[] getTerminalCommands();
}
