package ru.tsc.marfin.tm.api.model;

import ru.tsc.marfin.tm.enumerated.Status;

public interface IHasStatus {

    Status getStatus();

    void setStatus(Status status);

}
